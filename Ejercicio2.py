
def menu():
    print('1) Registrar productos')
    print('2) Mostrar listado de productos')
    print('3) Mostrar productos cuyo stock se encuentre [desde,hasta]')
    print('4) Sumar X al stock de productos con stock menor a Y')
    print('5) Eliminar productos con stock 0')
    print('6) Salir...')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una opción: '))

    return eleccion
    
def validarPrecio():
    precio = float(input('Precio: '))
    while not(precio >= 0):
        print('El precio NO es valido')
        precio = float(input('Precio: '))
    return precio
    
def validarStock():
    stock = int(input('Stock: '))
    while not(stock >= 0):
        print('Stock no valido')
        stock = int(input('Stock: '))
    return stock    
    
def registrarProductos():
    print('Cargar lista de productos')
    continua = 's'
    while (continua == 's'):
        codigo = int(input('Codigo: '))
        if codigo not in productos:
            descripcion = input('Descripcion: ')
            precio = validarPrecio()
            stock = validarStock()
            productos[codigo] = [descripcion,precio,stock]
            print('Producto agregado correctamente')
        else:
            print('El producto ya existe')
        continua = input('Desea cargar otro producto? s/n: ')
            
    return productos
    
def mostrar(diccionario):
    print('Listado de productos')
    for codigo, datos in diccionario.items():
        print(codigo,datos)
        
def validarHasta(hasta,desde):
    valido = False
    if (hasta > desde):
        valido = True
    return valido 

def mostrarIntervalo(productos):
    desde = int(input('Desde: '))
    hasta = int(input('Hasta: '))
    while not validarHasta(hasta,desde):
        print('Hasta debe ser mayor a Desde')
        hasta = int(input('Hasta: '))
    for codigo,datos in productos.items():
        if ((datos[2] >= desde) and (datos[2] <= hasta)):
            print(codigo,datos)
        
def procesoSuma(productos):
    x = int(input('Ingrese X: '))
    y = int(input('Ingrese Y: '))
    for codigo,datos in productos.items():
        if (datos[2] < y):
            datos[2] += x
            print(codigo,datos)
    
def eliminar(productos):
    for codigo,datos in list(productos.items()):
        if (datos[2] == 0):
            print('Se va a eliminar: ', productos[codigo])
            confirmacion = input('Confirma eliminarlo? s/n: ')
            if (confirmacion == 's'):
                del productos[codigo]
                print ('Eliminado ...')
                
def presionarTecla():
   input('Presione una tecla...')
                
def salir():
    print('fin del programa')
    
#principal
opcion = 0
productos = {}
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = registrarProductos()
    elif opcion == 2:
        mostrar(productos)
    elif opcion == 3:
        mostrarIntervalo(productos)
    elif opcion == 4:
        procesoSuma(productos)
    elif opcion == 5:
        eliminar(productos)
    elif opcion == 6:
        salir()
    presionarTecla()